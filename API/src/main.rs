use std::fs::File;
use std::io::prelude::*;
use std::env;

struct Carta {
	imagen: String,
	nombre: String,
	descripcion: String,
}
impl Carta {
	fn to_string(&self) {
		println!("Imagen: {}, nombre: {}, descripcion: {}", 
			self.imagen, self.nombre, self.descripcion);
	}
	fn to_html(&self) -> String {
		let pagina_carta = format!("<html><body><h1>{}</h1><br><img src=\"{}\" alt=\"Ilustracion\"/><br><p>{}</p></body></html>", 
			self.nombre, self.imagen,self.descripcion);
		pagina_carta
	}
	fn nombre(&self) -> &String {
		&self.nombre
	}
}
fn main() {
	let args: Vec<String> = env::args().collect();
 	let asesino = Carta {
 		imagen: String::from("\\resources\\asesino.png"),
 		nombre: String::from("asesino"),
 		descripcion: String::from("Anuncia el nombre del PERSONAJE que quieres asesinar. 
 			El asesinado pierde un turno"),
 	};
 	let ladron = Carta {
 		imagen: String::from("\\resources\\asesino.png"),
 		nombre: String::from("ladron"),
 		descripcion: String::from("Anuncia el nombre del PERSONAJE al que quiere robar. 
 			Cuando el personaje robado sea revelado, coge todas sus Monedas."),
 	};
 	let mago = Carta {
 		imagen: String::from("\\resources\\asesino.png"),
 		nombre: String::from("mago"),
 		descripcion: String::from("Intercambia las cartas de tu mano por las de otro JUGADOR, 
 			o descartate de cualquier numero de cartas para robar despues el mismo numero."),
 	};
 	let rey = Carta {
 		imagen: String::from("\\resources\\asesino.png"),
 		nombre: String::from("rey"),
 		descripcion: String::from("Coge la Corona. Gana 1 moneda de Oro por cada distrito NOBLE."),
 	};
 	let obispo = Carta {
 		imagen: String::from("\\resources\\asesino.png"),
 		nombre: String::from("obispo"),
 		descripcion: String::from("El Condotiero no puede destruir tus distritos. 
 			Gana 1 moneda por cada distrito Religioso"),
 	};
 	let mercader = Carta {
 		imagen: String::from("\\resources\\asesino.png"),
 		nombre: String::from("mercader"),
 		descripcion: String::from("Gana 1 moneda adicional. Gana 1 Moneda por cada uno de los distritos
 			comerciales."),
 	};
 	let arquitecto = Carta {
 		imagen: String::from("\\resources\\asesino.png"),
 		nombre: String::from("arquitecto"),
 		descripcion: String::from("Roba 2 cartas adicionales. Puedes construir 3 distritos."),
 	};
 	let condotiero = Carta {
 		imagen: String::from("\\resources\\asesino.png"),
 		nombre: String::from("condotiero"),
 		descripcion: String::from("Destruye 1 distrito pagando 1 moneda menos de su coste.
 			Gana 1 moneda por cada uno de los distritos militares."),
 	};
 	match args[1].as_str() {
		 "asesino" => {
			create_webpage(asesino);
		 },
		 "ladron" => {
			create_webpage(ladron);
		 },
		 "mago" => {
			create_webpage(mago);
		 },
		 "rey" => {
			create_webpage(rey);
		 },
		 "obispo" => {
			create_webpage(obispo);
		 },
		 "mercader" => {
			create_webpage(mercader);
		 },
		 "arquitecto" => {
			create_webpage(arquitecto);
		 },
		 "condotiero" => {
			create_webpage(condotiero);
		 },
		 _ => {
			println!("No hay argumentos, se procede a crear el indice."); 
			create_index();
			return;
		 },
	}
}
fn create_webpage(carta: Carta) {
	carta.to_string();
	let composicion_carta = carta.to_html();
	let ruta_imagen = format!("WebPagesAPI/{}.html", carta.nombre());
	let mut pagina_carta = File::create(ruta_imagen)
		.expect("No se pudo crear la pagina de la carta.");
	pagina_carta.write_all(composicion_carta.as_bytes()).expect("Cant create card page");
}
fn create_index() {
	let etiquetas = format!("<?php   echo \"<html>\"; ?>
<?php   echo \"<h1>Ciudadelas API</h1>\"; ?>
<?php   echo \"<br><h2>Seleccione personaje</h2>\"; ?>
<select name=\"personaje\">
        <option value=\"asesino\">Asesino</option>
        <option value=\"ladron\">Ladron</option>
        <option value=\"mago\">Mago</option>
        <option value=\"rey\">Rey</option>
        <option value=\"obispo\">Obispo</option>
        <option value=\"mercader\">Mercader</option>
        <option value=\"arquitecto\">Arquitecto</option>
        <option value=\"condotiero\">Condotiero</option>
</select>
<input type=\"button\" value=\"Seleccionar personaje\" class=\"homebutton\" id=\"tipo\" onClick=\"<?php shell_exec('cd /media/igle/HDD/Personal\\ Projects/Rust/hsapi/API/ ; cargo run $personaje');?>\"></input>
<?php   echo \"</html>\"; ?>");
	let mut index = File::create("WebPagesAPI/index.php").expect("No se pudo crear");
	index.write_all(etiquetas.as_bytes()).expect("Cant create index.");
}